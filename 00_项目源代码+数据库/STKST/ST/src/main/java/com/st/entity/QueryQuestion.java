package com.st.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/3/3
 * \* Time: 13:07
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class QueryQuestion {
    private Integer qid;
    private String content;
    private String rank;
    private String qtype;
    private Integer subject;
    private Integer rows = 1;//每页显示一条
    private Integer start;//偏移量
    private Integer page=1;
    private Integer previous;//上一页
    private Integer next;//下一页
    private Integer pagecount;//总页数

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getQtype() {
        return qtype;
    }

    public void setQtype(String qtype) {
        this.qtype = qtype;
    }

    public Integer getSubject() {
        return subject;
    }

    public void setSubject(Integer subject) {
        this.subject = subject;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPrevious() {
        return previous;
    }

    public void setPrevious(Integer previous) {
        this.previous = previous;
    }

    public Integer getNext() {
        return next;
    }

    public void setNext(Integer next) {
        this.next = next;
    }

    public Integer getPagecount() {
        return pagecount;
    }

    public void setPagecount(Integer pagecount) {
        this.pagecount = pagecount;
    }

    public QueryQuestion(Integer qid, String content, String rank, String qtype, Integer subject, Integer rows, Integer start, Integer page, Integer previous, Integer next, Integer pagecount) {
        this.qid = qid;
        this.content = content;
        this.rank = rank;
        this.qtype = qtype;
        this.subject = subject;
        this.rows = rows;
        this.start = start;
        this.page = page;
        this.previous = previous;
        this.next = next;
        this.pagecount = pagecount;
    }

    public QueryQuestion() {
    }

    @Override
    public String toString() {
        return "QueryQuestion{" +
                "qid=" + qid +
                ", content='" + content + '\'' +
                ", rank='" + rank + '\'' +
                ", qtype='" + qtype + '\'' +
                ", subject=" + subject +
                ", rows=" + rows +
                ", start=" + start +
                ", page=" + page +
                ", previous=" + previous +
                ", next=" + next +
                ", pagecount=" + pagecount +
                '}';
    }
}
