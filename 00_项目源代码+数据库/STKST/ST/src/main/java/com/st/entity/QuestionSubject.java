package com.st.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/3/3
 * \* Time: 13:06
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class QuestionSubject {
    private Integer sid;
    private String sname;

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public QuestionSubject() {
    }

    public QuestionSubject(Integer sid, String sname) {
        this.sid = sid;
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "QuestionSubject{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                '}';
    }
}
