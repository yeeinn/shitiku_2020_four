package com.st.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/3/3
 * \* Time: 10:42
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class Question {
    private Integer qid;
    private String content;
    private String A;
    private String B;
    private String C;
    private String D;
    private String rank;
    private String qtype;
    private Integer subject;
    private String answer;

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getQtype() {
        return qtype;
    }

    public void setQtype(String qtype) {
        this.qtype = qtype;
    }

    public Integer getSubject() {
        return subject;
    }

    public void setSubject(Integer subject) {
        this.subject = subject;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Question() {
    }

    public Question(Integer qid, String content, String a, String b, String c, String d, String rank, String qtype, Integer subject, String answer) {
        this.qid = qid;
        this.content = content;
        A = a;
        B = b;
        C = c;
        D = d;
        this.rank = rank;
        this.qtype = qtype;
        this.subject = subject;
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "Question{" +
                "qid=" + qid +
                ", content='" + content + '\'' +
                ", A='" + A + '\'' +
                ", B='" + B + '\'' +
                ", C='" + C + '\'' +
                ", D='" + D + '\'' +
                ", rank='" + rank + '\'' +
                ", qtype='" + qtype + '\'' +
                ", subject=" + subject +
                ", answer='" + answer + '\'' +
                '}';
    }
}
